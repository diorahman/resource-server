/*jslint node: true */
/*global exports */
'use strict';

/**
 * The client id and the client secret.  I'm using a
 * "trusted" client so that I don't get the "decision"
 * screen.
 */
exports.client = {
  clientID: "spse1234",
  clientSecret: "spsespse"
};

//TODO Compact this more, and document it better
/**
 * The Authorization server's location, port number, and the token info end point
 */

if (process.env.NODE_ENV === 'development') {
    exports.authorization = {
        host: "localhost",
        port: "3000",
        url: "http://localhost:3000/",
        tokenURL: "oauth/token",
        authorizeURL: "http://localhost:3000/dialog/authorize",
        tokeninfoURL: "http://localhost:3000/api/tokeninfo?access_token=",
        redirectURL: "http://localhost:4000/receivetoken"
    };
} else {
    exports.authorization = {
        host: "test.repl.rockybars.com",
        port: "443",
        url: "https://test.repl.rockybars.com/",
        tokenURL: "oauth/token",
        authorizeURL: "https://test.repl.rockybars.com/dialog/authorize",
        tokeninfoURL: "https://test.repl.rockybars.com/api/tokeninfo?access_token=",
        redirectURL: "https://spse.repl.rockybars.com/receivetoken"
    };
}

/**
 * Database configuration for access and refresh tokens.
 *
 * timeToCheckExpiredTokens - The time in seconds to check the database
 * for expired access tokens.  For example, if it's set to 3600, then that's
 * one hour to check for expired access tokens.
 * @type {{timeToCheckExpiredTokens: number}}
 */
exports.db = {
  timeToCheckExpiredTokens: 3600
};

/**
 * Session configuration
 *
 * secret - The session secret that you should change to what you want
 */
exports.session = {
  //TODO You need to change this secret to something that you choose for your secret
  secret: "A Secret That Should Be Changed"
};

exports.port = process.env.PORT || 4000;
exports.application = "SPSE";
