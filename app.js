/*jslint node: true */
'use strict';

var express = require('express');
var site = require('./site');
var passport = require('passport');
var fs = require('fs');
var http = require('http');
var config = require('./config');
var path = require('path');
var db = require('./db');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require("express-session");
var sso = require('./sso');

// Express configuration
var app = express();
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
app.use(cookieParser());
app.use(expressSession({
  saveUninitialized: true,
  resave: true,
  secret: config.session.secret
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

// Catch all for error messages.  Instead of a stack
// trace, this will log the json of the error message
// to the browser and pass along the status with it
app.use(function (err, req, res, next) {
  if (err) {
    res.status(err.status);
    res.json(err);
  } else {
    next();
  }
});

// Passport configuration
require('./auth');

app.get('/', site.index);
app.get('/login', site.loginForm);
app.post('/login', site.login);
app.get('/info', site.infosso);
app.get('/api/protectedEndPoint', site.protectedEndPoint);
app.get('/receivetoken', sso.receivetoken);

//static resources for stylesheets, images, javascript files
app.use(express.static(path.join(__dirname, 'public')));

//From time to time we need to clean up any expired tokens
//in the database
setInterval(function () {
  console.log("Checking for expired tokens");
  db.accessTokens.removeExpired(function (err) {
    if (err) {
      console.log("Error removing expired tokens");
    }
  });
}, config.db.timeToCheckExpiredTokens * 1000);

//This setting is so that our certificates will work although they are all self signed
//TODO Remove this if you are NOT using self signed certs

// Create our HTTPS server listening on port 4000.
http.createServer(app).listen(config.port);
console.log("Resource Server started on port " + config.port);

